<?php
namespace Coongeek\DataConverter\Service;

/**
 * Class ExtensionReaderService extracts file extension from file path.
 *
 * @package Coongeek\DataConverter\Service
 * @author Valentin Knyazev <coongeek@gmail.com>
 */
class ExtensionReaderService
{
    public function read($fromPath)
    {
        $fileParts = pathinfo($fromPath);
        $extension = $fileParts['extension'];

        return $extension;
    }
}