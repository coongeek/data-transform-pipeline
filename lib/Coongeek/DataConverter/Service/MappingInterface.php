<?php
namespace Coongeek\DataConverter\Service;

/**
 * Main purpose of this class is to provide information about accessible fields
 * of object.
 *
 * @package Coongeek\DataConverter\Service
 * @author Valentin Knyazev <coongeek@gmail.com>
 */
interface MappingInterface
{
    /**
     * Collects mapped field names for given class name.
     *
     * @param string $class
     * @return string[]
     */
    public function getMappedFields($class);

    /**
     * Creates getter method name string for given fieldName.
     *
     * @param string $fieldName
     * @return mixed
     */
    public function getAccessorForField($fieldName);

    /**
     * Creates setter method name string for given fieldName.
     * @param $fieldName
     * @return mixed
     */
    public function getSetterForField($fieldName);
}