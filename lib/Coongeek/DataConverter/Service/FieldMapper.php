<?php
namespace Coongeek\DataConverter\Service;

/**
 * Concrete implementaion of MappingInterface.
 *
 * @package Coongeek\DataConverter\Service
 */
class FieldMapper implements MappingInterface
{
    /**
     * Parameter $className should provide fully specified name of class
     * which mapped field names should be collected.
     *
     * @param string $className
     * @return string[]
     */
    public function getMappedFields($className)
    {
        $entityReflectionClass = new \ReflectionClass($className);
        $entityReflectionProperties = $entityReflectionClass->getProperties(\ReflectionProperty::IS_PRIVATE);
        $entityReflectionMethods = $entityReflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC);

        $probableFields = array();

        foreach ($entityReflectionProperties as $entityReflectionProperty) {
            $entityPropertyName = $entityReflectionProperty->getName();
            $probableFields[] = $entityPropertyName;
        }

        $probableFieldsWithAccessors = array();

        foreach ($entityReflectionMethods as $method) {
            $entityMethodName = $method->getShortName();
            $methodPrefix = substr($entityMethodName, 0, 3);

            if ($methodPrefix === 'get') {
                $probableField = strtolower(substr($entityMethodName, 3));
                $probableFieldsWithAccessors[] = $probableField;
            }
        }

        $probableFields = array_intersect($probableFields, $probableFieldsWithAccessors);

        return $probableFields;
    }

    public function getAccessorForField($fieldName)
    {
        return 'get' . strtoupper(substr($fieldName, 0, 1)) . substr($fieldName, 1);
    }

    public function getSetterForField($fieldName)
    {
        return 'set' . strtoupper(substr($fieldName, 0, 1)) . substr($fieldName, 1);
    }
}