<?php
namespace Coongeek\DataConverter;

use Coongeek\DataConverter\EntityLoader\EntityLoaderInterface;
use Coongeek\DataConverter\EntityPersister\EntityPersisterInterface;

/**
 * Class DataConverter converts file populated with entity data
 * to desired format.
 *
 * @package Coongeek\DataConverter
 * @author Valentin Knyazev <coongeek@gmail.com>
 */
class DataConverter
{
    /**
     * Loads array of entities of given type from given file.
     *
     * @var EntityLoaderInterface
     */
    private $entityLoader;
    /**
     * Persists array of entities of given type to given file.
     *
     * @var EntityPersisterInterface
     */
    private $entityPersister;
    /**
     * Sorts array of given entities by field in asc or desc directions.
     *
     * @var EntitySorter
     */
    private $entitySorter;
    /**
     * Fully specified class name of entities which will be stored into new format.
     *
     * @var string
     */
    private $className;

    public function __construct($className, EntityLoaderInterface $entityLoader, EntityPersisterInterface $entityPersister, EntitySorter $entitySorter = null)
    {
        $this->className = $className;
        $this->entityLoader = $entityLoader;
        $this->entityPersister = $entityPersister;
        $this->entitySorter = $entitySorter;
    }

    /**
     * Loads entities of type which is specified by DataConverter
     * itself from given path, sorts them if EntitySorter object is present
     * and both field and direction parameters are provided, and then persists
     * those entities into desired format.
     *
     * @param string $path
     * @param string $format
     * @param string|null $field
     * @param string|null $direction
     */
    public function convert($path, $format, $field = null, $direction = null)
    {
        $entities = $this->entityLoader->load($this->className, $path);

        if (!is_null($this->entitySorter) && strlen($field) > 0 && in_array(strtolower($direction), array('asc', 'desc'))) {
            $entities = $this->entitySorter->sort($entities, $field, $direction);
        }

        $convertedFilePath = $this->getConvertedFilePath($path, $format);

        $this->entityPersister->persist($entities, $convertedFilePath, $this->className);
    }

    private function getConvertedFilePath($path, $format)
    {
        $info = pathinfo($path);
        $convertedFilePath = $info['dirname'] . "/" . $info['filename'] . '.' . $format;

        return $convertedFilePath;
    }
}