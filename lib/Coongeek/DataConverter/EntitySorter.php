<?php
namespace Coongeek\DataConverter;

use Coongeek\DataConverter\Service\MappingInterface;
use Coongeek\DataConverter\Service\FieldMapper;

/**
 * Class EntitySorter performs sorting of given array populated by entities
 * by given entity class field name and sorting direction.
 *
 * @package Coongeek\DataConverter
 * @author Valentin Knyazev <coongeek@gmail.com>
 */
class EntitySorter
{
    private $className;
    private $mapping;
    private $field;

    public function __construct($className, MappingInterface $mapping = null)
    {
        $this->className = $className;
        $this->mapping = (is_null($mapping)) ? new FieldMapper() : $mapping;
    }

    /**
     * Return copy of provided entities array.
     *
     * @param object[] $entities
     * @param string $field
     * @param string $direction
     *
     * @return object[]
     */
    public function sort($entities, $field, $direction)
    {
        $this->field = $field;

        if (strtolower($direction) === 'asc') {
            $callback = "sortAsc";
        } else if (strtolower($direction) === 'desc') {
            $callback = "sortDesc";
        } else {
            throw new Exception("Wrong sorting direction. Available direction options are ASC and DESC.");
        }


        usort($entities, array($this, $callback));

        return $entities;
    }

    /**
     * Obtain accessor function reflection of current sort field.
     *
     * @return \ReflectionMethod
     * @throws \ReflectionException
     */
    private function getReflectionAccessorMethod()
    {
        $fieldAccessorMethod = $this->mapping->getAccessorForField($this->field);

        return new \ReflectionMethod($this->className, $fieldAccessorMethod);
    }

    /**
     * usort() sorting callback, ascending order - compares values of sort field
     * for given objects.
     *
     * @param $entity1
     * @param $entity2
     * @return int
     * @throws \ReflectionException
     */
    private function sortAsc($entity1, $entity2)
    {
        $entityReflectionMethod = $this->getReflectionAccessorMethod();
        $entity1FieldValue = $entityReflectionMethod->invoke($entity1);
        $entity2FieldValue = $entityReflectionMethod->invoke($entity2);

        return strcmp($entity1FieldValue, $entity2FieldValue);
    }

    /**
     * usort() sorting callback, descending order - compares values of sort field
     * for given objects.
     *
     * @param $entity1
     * @param $entity2
     * @return int
     * @throws \ReflectionException
     */
    private function sortDesc($entity1, $entity2)
    {
        $entityReflectionMethod = $this->getReflectionAccessorMethod();
        $entity1FieldValue = $entityReflectionMethod->invoke($entity1);
        $entity2FieldValue = $entityReflectionMethod->invoke($entity2);

        return strcmp($entity2FieldValue, $entity1FieldValue);
    }
}