<?php
namespace Coongeek\DataConverter\EntityLoader;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Coongeek\DataConverter\EntityLoader\EntityLoaders\CsvEntityLoader;
use Coongeek\DataConverter\Service\ExtensionReaderService;

/**
 * EntityLoader class.
 *
 * @author Valentin Knyazev <coongeek@gmail.com>
 * @package Coongeek\DataConverter\EntityLoader
 */
class EntityLoader implements EntityLoaderInterface
{
    private $loaders = array();
    private $extensionReader;

    public function __construct(ValidatorInterface $validator = null, ExtensionReaderService $extensionReader = null)
    {
        $this->loaders['csv'] = new CsvEntityLoader($validator);
        $this->extensionReader = (is_null($extensionReader)) ? new ExtensionReaderService() : $extensionReader;
    }

    /**
     * Allows to overwrite loader of given format or add
     * loader with format which yet have not been present.
     *
     * @param EntityLoaderInterface $hotelLoader
     * @param string $format
     */
    public function setLoader(EntityLoaderInterface $hotelLoader, $format)
    {
        $this->loaders[$format] = $hotelLoader;
    }

    /**
     * Extracts format from given path, picks corresponding
     * loader and transmits it retrieving of entities of
     * specified types.
     *
     * @param string $className
     * @param string $path
     * @return object[]
     */
    public function load($className, $path)
    {
        $extension = $this->extensionReader->read($path);

        if (array_key_exists($extension, $this->loaders)) {
            $loader = $this->loaders[$extension];
            $entities = $loader->load($className, $path);
        } else {
            throw new Exception('Loader for specified type of file was not found.');
        }

        return $entities;
    }
}