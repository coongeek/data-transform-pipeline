<?php
namespace Coongeek\DataConverter\EntityLoader;

/**
 * Interface EntityLoaderInterface
 *
 * @package Coongeek\DataConverter\EntityLoader
 * @author Valentin Knyazev <coongeek@gmail.com>
 */
interface EntityLoaderInterface
{
    /**
     * Loads array of entities with given type from given data source.
     * First parameter should be fully specified class name.
     *
     * @param string $path
     * @return object[]
     */
    public function load($className, $path);
}