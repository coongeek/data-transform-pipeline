<?php
namespace Coongeek\DataConverter\EntityLoader\EntityLoaders;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Validation;
use Coongeek\DataConverter\EntityLoader\EntityLoaderInterface;
use Coongeek\DataConverter\Service\MappingInterface;
use Coongeek\DataConverter\Service\FieldMapper;

/**
 * Load entities of specified type from given csv file.
 *
 * @author ValentinKnyazev <coongeek@gmail.com>
 * @package Coongeek\DataConverter\EntityLoader\EntityLoaders
 */
class CsvEntityLoader implements EntityLoaderInterface
{
    /**
     * If not provided default validator which validates
     * against loadValidatorMetadata method of the object is created.
     *
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * Extracts meta containing information about accessible fields
     * of object which be attempted to load from csv scheme.
     *
     * @var FieldMapper|MappingInterface
     */
    private $mapping;

    public function __construct(ValidatorInterface $validator = null, MappingInterface $mapping = null)
    {

        if (is_null($validator)) {
            $this->validator = Validation::createValidatorBuilder()
                ->addMethodMapping('loadValidatorMetadata')
                ->getValidator();
        } else {
            $this->validator = $validator;
        }

        $this->mapping = (is_null($mapping)) ? new FieldMapper() : $mapping;
    }

    /**
     * Returns array populated by entities of given entity type
     * loaded from given csv file.
     *
     * @param string $className
     * @param string $path
     * @return object[]
     */
    public function load($className, $path)
    {
        $file = fopen($path, 'r');

        $isFirstLine = true;
        $fieldPositions = null;

        $entities = array();

        while (($line = fgetcsv($file)) !== false) {
            if ($isFirstLine) {
                for ($i = 0; $i < count($line); $i++) {
                    $fieldPositions = $line;
                }

                $isFirstLine = false;
                continue;
            }

            $entityReflectionClass = new \ReflectionClass($className);
            $entity = $entityReflectionClass->newInstance();

            for ($i = 0; $i < count($line); $i++) {
                $fieldName = $fieldPositions[$i];
                $fieldValue = $line[$i];

                $setterMethod = $this->mapping->getSetterForField($fieldName);
                $reflectionMethod = new \ReflectionMethod($className, $setterMethod);
                $reflectionMethod->invoke($entity, $fieldValue);
            }

            $entityIsValid = $this->validator->validate($entity);

            if ($entityIsValid) {
                $entities[] = $entity;
            }
        }

        return $entities;
    }
}