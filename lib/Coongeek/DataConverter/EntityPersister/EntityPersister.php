<?php
namespace Coongeek\DataConverter\EntityPersister;

use Coongeek\DataConverter\Service\ExtensionReaderService;
use Coongeek\DataConverter\EntityPersister\EntityPersisters\EntityJsonPersister;
use Coongeek\DataConverter\EntityPersister\EntityPersisters\EntityXMLPersister;

/**
 * EntityPersister class.
 *
 * @package Coongeek\DataConverter\EntityPersister
 * @author Valentin Knyazev <coongeek@gmail.com>
 */
class EntityPersister implements EntityPersisterInterface
{
    private $entityPersisters = array();
    private $extensionReader;

    /**
     * First parameter must be fully specified class name.
     * It defines explicit contract between default persisters
     * and type of entities which they could manage.
     *
     * @param string $className
     * @param ExtensionReaderService|null $extensionReader
     */
    public function __construct(ExtensionReaderService $extensionReader = null)
    {
        $this->extensionReader = (is_null($extensionReader)) ? new ExtensionReaderService() : $extensionReader;
        $this->entityPersisters["json"] = new EntityJsonPersister();
        $this->entityPersisters["xml"] = new EntityXMLPersister();
    }

    /**
     * Set given persister for specified format.
     *
     * @param EntityPersisterInterface $persister
     * @param string $format
     */
    public function setPersister(EntityPersisterInterface $persister, $format)
    {
        $this->entityPersisters[$format] = $persister;
    }

    /**
     * Extracts format from given path, picks corresponding
     * persister and transmits it persisting of entities of
     * specified types.
     *
     * @param object[] $entities
     * @param string $folderPath
     * @param string $className
     * @throws \Exception
     */
    public function persist($entities, $folderPath, $className)
    {
        $format = $this->extensionReader->read($folderPath);
        if (array_key_exists($format, $this->entityPersisters)) {
            $entityPersister = $this->entityPersisters[$format];
            $entityPersister->persist($entities, $folderPath, $className);
        } else {
            throw new \Exception('Persister not found for specified format.');
        }
    }
}