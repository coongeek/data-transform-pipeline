<?php
namespace Coongeek\DataConverter\EntityPersister\EntityPersisters;

use Coongeek\DataConverter\EntityPersister\EntityPersisterInterface;
use Coongeek\DataConverter\Service\MappingInterface;
use Coongeek\DataConverter\Service\FieldMapper;

/**
 * Concrete implementation of EntityJsonPersisterInterface.
 *
 * @package Coongeek\DataConverter\EntityPersister\EntityPersisters
 * @author ValentinKnyazev <coongeek@gmail.com>
 */
class EntityJsonPersister implements EntityPersisterInterface
{
    private $mapping;

    public function __construct(MappingInterface $mapping = null)
    {
        $this->mapping = (is_null($mapping)) ? new FieldMapper() : $mapping;
    }

    /**
     * @param object[] $entities
     * @param string $format
     * @param string $path
     */
    public function persist($entities, $path, $className)
    {
        $probableFields = $this->mapping->getMappedFields($className);
        $entityArray = array();

        foreach ($entities as $entity) {
            $entityReflectionObject = new \ReflectionObject($entity);

            $propertyValueArray = array();
            foreach ($probableFields as $field) {
                $restoredMethodName = $this->mapping->getAccessorForField($field);
                $entityReflectionMethod = $entityReflectionObject->getMethod($restoredMethodName);
                $methodValue = $entityReflectionMethod->invoke($entity);

                $propertyValueArray[$field] = $methodValue;
            }

            $entityArray[] = $propertyValueArray;
        }

        $hotelsJson = json_encode($entityArray, JSON_UNESCAPED_UNICODE);

        file_put_contents($path, $hotelsJson);
    }
}