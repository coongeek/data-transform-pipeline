<?php
namespace Coongeek\DataConverter\EntityPersister\EntityPersisters;

use Coongeek\DataConverter\EntityPersister\EntityPersisterInterface;
use Coongeek\DataConverter\Service\MappingInterface;
use Coongeek\DataConverter\Service\FieldMapper;


/**
 * Concrete implementation of EntityPersisterInterface.
 *
 * @package Coongeek\DataConverter\EntityPersister\EntityPersisters
 * @author Valentin Knyazev <coongeek@gmail.com>
 */
class EntityXMLPersister implements EntityPersisterInterface
{
    private $mapping;

    public function __construct(MappingInterface $mapping = null)
    {
        $this->mapping = (is_null($mapping)) ? new FieldMapper()  : $mapping;
    }

    /**
     * Should accept persist configurator instead.
     *
     * @param object[] $entities
     * @param string $path
     * @param string $className
     * @throws \ReflectionException
     */
    public function persist($entities, $path, $className)
    {
        $entityFields = $this->mapping->getMappedFields($className);

        $shortClassName = $this->getShortClassName($className);
        $rootNodeName = $this->getRootNodeForClass($shortClassName);
        $elementNodeName = $this->getElementNodeForClass($shortClassName);

        $xml = new \DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $root = $xml->appendChild(
            $xml->createElement($rootNodeName)
        );

        foreach ($entities as $entity) {
            $elementTag = $root->appendChild(
                $xml->createElement($elementNodeName)
            );

            foreach ($entityFields as $entityField) {
                $fieldAccessor = $this->mapping->getAccessorForField($entityField);
                $entityReflectionClass = new \ReflectionClass($className);
                $entityReflectionMethod = $entityReflectionClass->getMethod($fieldAccessor);
                $entityFieldValue = $entityReflectionMethod->invoke($entity);

                $elementTag->appendChild(
                    $xml->createAttribute($entityField)
                )->appendChild(
                    $xml->createTextNode($entityFieldValue)
                );
            }
        }


        $xmlStr = $xml->saveXML();
        file_put_contents($path, $xmlStr);
    }

    /**
     * Given parameter should represent by fully specified class name.
     *
     * @param string $className
     * @return string
     */
    public function getShortClassName($className)
    {
        return substr(strrchr($className, '\\'), 1);
    }
    /**
     * Given parameter should be short class name of
     * entities which would persisted in xml file. Class name
     * should be present by it's singular form. This method returns it's lowercase plural form
     * to be used as root XML tree node name.
     *
     * @param string $className
     * @return string
     */
    public function getRootNodeForClass($className)
    {
        return strtolower($className) . 's';
    }

    /**
     * Given parameter should be short class name of
     * entities to be persisted in xml file. Class name
     * should be present by it's singular form. Method will return
     * lowercase singular form of the class name to be used as name of xml
     * tree node representing single entity from the larger set defined by
     * getRootNodeForClass
     *
     * @param $className
     * @return string
     */
    public function getElementNodeForClass($className)
    {
        return strtolower($className);
    }
}