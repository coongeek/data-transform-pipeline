<?php
namespace Coongeek\DataConverter\EntityPersister;

/**
 * Interface EntityPersisterInterface
 *
 * @package Coongeek\DataConverter\EntityPersister
 * @author Valentin Knyazev <coongeek@gmail.com>
 */
interface EntityPersisterInterface
{
    /**
     * Implementation of this method must create a file of specified format
     * which is defined by file path, containing represenation of array of
     * entities with given className.
     * $className should be represented by fully specified class name.
     *
     * @param object[] $entities
     * @param string $path
     * @param string $className
     * @return void
     */
    public function persist($entities, $path, $className);
}