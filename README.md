# Data Pipeline Case-Study Solution

## Installation and usage

1. This application requires at least PHP 5.6 version. All required dependencies
   are managed by composer and already installed inside vendor folder.

2. You could run data converter by excecuting following console command from inside data-transform-pipeline folder:
   php web/application.php app:convert-hotels-data path\to\hotels\data\file format field direction
   
   Argumetns:
   
   path - required - must be represented by full path to file containing hotels data
   format - required - one of the supported conversion formats - xml or json
   field - optional - name of the field by which hotelds data will be sorted
   direction - optional - direction of the sorting - either asc or desc 
   
   Note that sorting won't work if only field parameter is presented.

   Example usage:
   
   c:\projects\data-transform-pipeline>php web/application.php app:convert-hotels-data C:\projects\data-transform-pipeline\data\hotels.csv xml stars asc

## Notes on the task

The central class of the solution is Coongeek\DataConverter\DataConverter. It is
capable of loading and converting data through usage of Coongeek\DataConverter\EntityLoader\EntityLoader and
Coongeek\DataConverter\EntityPersister\EntityPersister classes, which contains collections
of loaders and persister for different file formats. EntityLoader and EntityPersister
are agnostic to type of entity(achieved by usage of reflection) and can manage any object mapped by Coongeek\DataConverter\Service\FieldMapper instance
or any other mapper implementing Coongeek\DataConverter\Service\MappingInterface. They also 
can be easily extended by new loaders and persisters through usage of interfaces, setPersister and setLoader methods.
The entities could also be sorted before they will be stored inside file of new format,
it is achieved by usage of Coongeek\DataConverter\EntitySorter object.

Validation is performed by usage of symfony/validator library, tests for validator
methods could be found inside src/HotelDataConverter/Test folder.