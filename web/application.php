<?php

$autoloader = require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\Console\Application;
use Coongeek\HotelDataConverter\Command\ConvertHotelsDataCommand;

$application = new Application();
$application->add(new ConvertHotelsDataCommand());
$application->run();