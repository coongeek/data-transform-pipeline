<?php
namespace Coongeek\HotelDataConverter\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsValidUTF8StringValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$this->isValidUTF8String($value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

    public function isValidUTF8String($value)
    {
        return mb_check_encoding($value, 'UTF-8');
    }
}