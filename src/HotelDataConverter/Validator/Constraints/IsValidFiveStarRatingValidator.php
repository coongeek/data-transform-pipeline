<?php
namespace Coongeek\HotelDataConverter\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsValidFiveStarRatingValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$this->isValidFiveStarRating($value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

    public function isValidFiveStarRating($value)
    {
        return is_numeric($value) && $value >= 1 && $value <= 5;
    }
}