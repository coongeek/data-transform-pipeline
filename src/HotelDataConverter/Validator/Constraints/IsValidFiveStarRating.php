<?php
namespace Coongeek\HotelDataConverter\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsValidFiveStarRating extends Constraint
{
    public $message = "The rating must contain numeric value between 1 and 5.";
}