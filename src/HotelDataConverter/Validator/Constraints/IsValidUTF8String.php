<?php
namespace Coongeek\HotelDataConverter\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsValidUTF8String extends Constraint
{
    public $message = 'String could contain only valid UTF-8 characters.';
}