<?php
namespace Coongeek\HotelDataConverter\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsValidHttpURLValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$this->isValidHttpURL($value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

    public function isValidHttpURL($value)
    {
        $result = preg_match('/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/', $value);
        return (bool)$result;
    }
}