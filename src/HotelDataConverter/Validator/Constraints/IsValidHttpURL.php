<?php
namespace Coongeek\HotelDataConverter\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class IsValidHttpURL extends Constraint
{
    public $message = "Provided string is not valid http url.";
}