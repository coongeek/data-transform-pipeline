<?php
namespace Coongeek\HotelDataConverter\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Coongeek\HotelDataConverter\Entity\Hotel;
use Coongeek\DataConverter\EntityLoader\EntityLoader;
use Coongeek\DataConverter\EntityPersister\EntityPersister;
use Coongeek\DataConverter\EntitySorter;
use Coongeek\DataConverter\DataConverter;

class ConvertHotelsDataCommand extends Command
{
    protected function configure()
    {
        $this->setName('app:convert-hotels-data');
        $this->setDescription('Convert hotels data stored in given file to file of specified format.');
        $this->addArgument('path', InputArgument::REQUIRED, 'The path to file to be converted.');
        $this->addArgument('format', InputArgument::REQUIRED, 'Format to which file will be converted.');
        $this->addArgument('field', InputArgument::OPTIONAL);
        $this->addArgument('direction', InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            "Hotels data converter",
            "=====================",
            ""
        ]);

        $hotel = new Hotel();
        $hotelClass = get_class($hotel);
        $hotelLoader = new EntityLoader();
        $hotelPersister = new EntityPersister();

        $entitySorter = new EntitySorter($hotelClass);
        $hotelDataConverter = new DataConverter($hotelClass, $hotelLoader, $hotelPersister, $entitySorter);

        $path = $input->getArgument('path');
        $format = $input->getArgument('format');
        $field = $input->getArgument('field');
        $direction = $input->getArgument('direction');
        $hotelDataConverter->convert($path, $format, $field, $direction);
        $output->writeln('Hotels data was successfully converted to ' . $format . ' format.');
    }
}