<?php
namespace Coongeek\HotelDataConverter\Entity;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Coongeek\HotelDataConverter\Validator\Constraints as CoongeekAssert;

/**
 * Class Hotel represents hotel data.
 *
 * @package Coongeek\HotelDataConverter\Entity
 * @author Valentin Knyazev <coongeek@gmail.com>
 */
class Hotel
{
    private $name;
    private $address;
    private $stars;
    private $contact;
    private $phone;
    private $uri;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Hotel
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Hotel
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStars()
    {
        return $this->stars;
    }

    /**
     * @param mixed $stars
     * @return Hotel
     */
    public function setStars($stars)
    {
        $this->stars = $stars;
        return $this;
    }

    /**
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param string $contact
     * @return Hotel
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return Hotel
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param Hotel $uri
     * @return Hotel
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * This method defines constraints against which current instance
     * could be checked by instance of Symfony\Component\Validator\Validator\ValidatorInterface.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('name', new CoongeekAssert\IsValidUTF8String());
        $metadata->addPropertyConstraint('stars', new CoongeekAssert\IsValidFiveStarRating());
        $metadata->addPropertyConstraint('uri', new CoongeekAssert\IsValidHttpURL());
    }
}