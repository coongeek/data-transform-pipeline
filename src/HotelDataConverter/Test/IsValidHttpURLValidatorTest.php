<?php
namespace Coongeek\Test;

use PHPUnit\Framework\TestCase;
use Coongeek\HotelDataConverter\Validator\Constraints\IsValidHttpURLValidator;

class IsValidHttpURLValidatorTest extends TestCase
{
    public function testIsValidHttpURL()
    {
        $validator = new IsValidHttpURLValidator();
        $this->assertTrue($validator->isValidHttpURL("http://www.garden.fr/"));
        $this->assertTrue($validator->isValidHttpURL("https://www.garden.fr/"));
        $this->assertTrue($validator->isValidHttpURL("https://www.garden.fr"));
        $this->assertTrue($validator->isValidHttpURL("https://garden.fr/"));
        $this->assertTrue($validator->isValidHttpURL("https://garden.fr"));
        $this->assertTrue($validator->isValidHttpURL("www.garden.fr"));
        $this->assertTrue($validator->isValidHttpURL("garden.fr"));
        $this->assertTrue($validator->isValidHttpURL("garden.fr/index.html"));
        $this->assertTrue($validator->isValidHttpURL("http://garden.fr/category/about/"));
        $this->assertTrue($validator->isValidHttpURL("http://garden.fr/category/about/page.php"));
        $this->assertFalse($validator->isValidHttpURL("htt://garden.fr"));
        $this->assertFalse($validator->isValidHttpURL("http:/garden.fr"));
        $this->assertFalse($validator->isValidHttpURL("http://garden"));
    }
}