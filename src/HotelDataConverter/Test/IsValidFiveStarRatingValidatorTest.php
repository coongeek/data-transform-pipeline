<?php
namespace Coongeek\Test;

use PHPUnit\Framework\TestCase;
use Coongeek\HotelDataConverter\Validator\Constraints\IsValidFiveStarRatingValidator;

class IsValidFiveStarRatingValidatorTest extends TestCase
{
    public function testIsValidFiveStarRating()
    {
        $validator = new IsValidFiveStarRatingValidator();

        $this->assertTrue($validator->isValidFiveStarRating(1));
        $this->assertTrue($validator->isValidFiveStarRating(5));
        $this->assertTrue($validator->isValidFiveStarRating("3.5"));
        $this->assertFalse($validator->isValidFiveStarRating(0.99));
        $this->assertFalse($validator->isValidFiveStarRating(6));
        $this->assertFalse($validator->isValidFiveStarRating(true));
        $this->assertFalse($validator->isValidFiveStarRating("3abc"));
    }
}