<?php
namespace Coongeek\Test;

use PHPUnit\Framework\TestCase;
use Coongeek\HotelDataConverter\Validator\Constraints\IsValidUTF8StringValidator;

class IsValidUTF8StringValidatorTest extends TestCase
{
    public function testIsValidUTF8String()
    {
        $validator = new IsValidUTF8StringValidator();
        $this->assertTrue($validator->isValidUTF8String("Dörschner Wagner"));
        $this->assertTrue($validator->isValidUTF8String('κόσμε'));
        $this->assertFalse($validator->isValidUTF8String("\xc3\x28"));
        $this->assertFalse($validator->isValidUTF8String("\xf0\x28\x8c\xbc"));
    }
}